package com.morozov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class AbstractEntity {
    @NotNull
    private String id = UUID.randomUUID().toString();
}
