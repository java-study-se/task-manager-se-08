package com.morozov.tm.entity;

import com.morozov.tm.security.role.UserRoleEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {
    @NotNull
    private String login = "";
    @NotNull
    private String passwordHash = "";
    @NotNull
    private UserRoleEnum role = UserRoleEnum.USER;
}
