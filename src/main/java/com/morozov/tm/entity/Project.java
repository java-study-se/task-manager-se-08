package com.morozov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractEntity {
    @NotNull
    private String name = "";
    @NotNull
    private String description = "";
    @NotNull
    private String userId = "";
    @Nullable
    private Date startDate = null;
    @Nullable
    private Date endDate = null;

    @Override
    public final String toString() {
        return "Project{" +
                "id='" + getId() + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + userId + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
