package com.morozov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractEntity {
    @NotNull
    private String name = "";
    @NotNull
    private String description = "";
    @Nullable
    private Date startDate = null;
    @Nullable
    private Date endDate = null;
    @NotNull
    private String idProject = "";
    @NotNull
    private String userId = "";

    @Override
    public final String toString() {
        return "Task{" +
                "id='" + getId() + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", idProject='" + idProject + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
