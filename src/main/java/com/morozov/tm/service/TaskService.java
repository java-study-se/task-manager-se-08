package com.morozov.tm.service;

import com.morozov.tm.api.ITaskRepository;
import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.DataValidationUtil;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.util.List;


public final class TaskService {

    final private ITaskRepository taskRepository;

    public TaskService(@NotNull ITaskRepository TaskRepository) {
        this.taskRepository = TaskRepository;
    }

    @NotNull
    public List<Task> getAllTaskByUserId(@NotNull final String userId) throws RepositoryEmptyException {
        @NotNull final List<Task> taskList = taskRepository.findAllByUserId(userId);
        DataValidationUtil.checkEmptyRepository(taskList);
        return taskList;
    }

    @NotNull
    public Task addTask(@NotNull final String userId,
                        @NotNull final String taskName,
                        @NotNull final String taskDescription,
                        @NotNull final String dataStart,
                        @NotNull final String dataEnd,
                        @NotNull final String projectId) throws StringEmptyException, ParseException {
        DataValidationUtil.checkEmptyString(taskName, taskDescription, dataStart, dataEnd, projectId);
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setStartDate(ConsoleHelperUtil.formattedData(dataStart));
        task.setEndDate(ConsoleHelperUtil.formattedData(dataEnd));
        task.setIdProject(projectId);
        task.setUserId(userId);
        taskRepository.persist(task.getId(), task);
        return task;
    }

    public boolean deleteTask(@NotNull final String userId, @NotNull final String id) throws StringEmptyException {
        boolean isDeleted = false;
        DataValidationUtil.checkEmptyString(id);
        if (taskRepository.findOneByUserId(userId, id) != null) {
            taskRepository.remove(id);
            isDeleted = true;
        }
        return isDeleted;
    }

    public void updateTask(@NotNull final String userId,
                           @NotNull final String id,
                           @NotNull final String name,
                           @NotNull final String description,
                           @NotNull final String startDate,
                           @NotNull final String endDate,
                           @NotNull final String projectId)
            throws RepositoryEmptyException, StringEmptyException, ParseException {
        DataValidationUtil.checkEmptyRepository(taskRepository.findAll());
        DataValidationUtil.checkEmptyString(id, name, description, startDate, endDate, projectId);
        @NotNull final Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setStartDate(ConsoleHelperUtil.formattedData(startDate));
        task.setEndDate(ConsoleHelperUtil.formattedData(endDate));
        task.setIdProject(projectId);
        task.setUserId(userId);
        taskRepository.merge(id, task);
    }

    @NotNull
    public List<Task> getAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId)
            throws StringEmptyException, RepositoryEmptyException {
        DataValidationUtil.checkEmptyString(projectId);
        @NotNull final List<Task> resultTaskList = taskRepository.findAllByProjectIdUserId(userId, projectId);
        DataValidationUtil.checkEmptyRepository(resultTaskList);
        return resultTaskList;
    }

    public void deleteAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        taskRepository.deleteAllTaskByProjectId(userId, projectId);
    }

    public void clearTaskList() {
        taskRepository.removeAll();
    }
}
