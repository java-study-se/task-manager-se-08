package com.morozov.tm.service;

import com.morozov.tm.api.IUserRepository;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.entity.User;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.DataValidationUtil;
import com.morozov.tm.util.MD5HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserService {

    final private IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository UserRepository) {
        this.userRepository = UserRepository;
    }
    @Nullable
    public User loginUser(@NotNull final String login,@NotNull final String password) throws UserNotFoundException, StringEmptyException {
        DataValidationUtil.checkEmptyString(login, password);
        @Nullable final User user = (User) userRepository.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final String typePasswordHash = MD5HashUtil.getHash(password);
        if (typePasswordHash.equals(user.getPasswordHash())) {
            return user;
        } else return null;
    }

    public void registryUser(@NotNull final String login,@NotNull final String password) throws UserExistException, StringEmptyException {
        DataValidationUtil.checkEmptyString(login, password);
        if (userRepository.findOneByLogin(login) != null) throw new UserExistException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setRole(UserRoleEnum.USER);
        user.setPasswordHash(MD5HashUtil.getHash(password));
        userRepository.persist(user.getId(), user);
    }

    public void updateUserPassword(@NotNull final String id,@NotNull final String newPassword) throws StringEmptyException, UserNotFoundException {
        @Nullable final User user = (User) userRepository.findOne(id);
        DataValidationUtil.checkEmptyString(newPassword);
        if(user == null) throw new UserNotFoundException();
        user.setPasswordHash(MD5HashUtil.getHash(newPassword));
        userRepository.merge(id, user);
    }

    public void updateUserProfile(@NotNull final String id,@NotNull final String newUserName,@NotNull final String newUserPassword) throws StringEmptyException, UserExistException, UserNotFoundException {
        @Nullable final User user = (User) userRepository.findOne(id);
        DataValidationUtil.checkEmptyString(newUserName, newUserPassword);
        if (userRepository.findOneByLogin(newUserName) != null) throw new UserExistException();
        if(user == null) throw new UserNotFoundException();
        user.setLogin(newUserName);
        user.setPasswordHash(MD5HashUtil.getHash(newUserPassword));
        userRepository.merge(id, user);
    }
}
