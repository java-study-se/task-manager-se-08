package com.morozov.tm.service;

import com.morozov.tm.api.IProjectRepository;
import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.DataValidationUtil;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.util.List;

public final class ProjectService {

    final private IProjectRepository projectRepository;

    public ProjectService(@NotNull IProjectRepository ProjectRepository) {
        this.projectRepository = ProjectRepository;
    }

    @NotNull
    public final List<Project> getAllProject() throws RepositoryEmptyException {
        @NotNull final List<Project> projectList = projectRepository.findAll();
        DataValidationUtil.checkEmptyRepository(projectList);
        return projectList;
    }
    @NotNull
    public final List<Project> getAllProjectByUserId(@NotNull String userId) throws RepositoryEmptyException {
        @NotNull final List<Project> projectListByUserId = projectRepository.findAllByUserId(userId);
        DataValidationUtil.checkEmptyRepository(projectListByUserId);
        return projectListByUserId;
    }

    public final Project addProject(@NotNull final String userId,
                                    @NotNull final String projectName,
                                    @NotNull final String projectDescription,
                                    @NotNull final String dataStart,
                                    @NotNull final String dataEnd)
            throws StringEmptyException, ParseException {
        DataValidationUtil.checkEmptyString(projectName, projectDescription, dataStart, dataEnd);
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setStartDate(ConsoleHelperUtil.formattedData(dataStart));
        project.setEndDate(ConsoleHelperUtil.formattedData(dataEnd));
        project.setUserId(userId);
        projectRepository.persist(project.getId(), project);
        return project;
    }

    public final boolean deleteProject(@NotNull final String userId, @NotNull String id) throws StringEmptyException {
        boolean isDeleted = false;
        DataValidationUtil.checkEmptyString(id);
        if (projectRepository.findOneByUserId(userId, id) != null) {
            projectRepository.remove(id);
            isDeleted = true;
        }
        return isDeleted;
    }

    public final void updateProject(@NotNull final String userId,
                                    @NotNull final String id,
                                    @NotNull final String projectName,
                                    @NotNull final String projectDescription,
                                    @NotNull final String dataStart,
                                    @NotNull final String dataEnd)
            throws RepositoryEmptyException, StringEmptyException, ParseException {
        DataValidationUtil.checkEmptyRepository(projectRepository.findAllByUserId(userId));
        DataValidationUtil.checkEmptyString(id, projectName, projectDescription, dataStart, dataEnd);
        @NotNull final Project project = new Project();
        project.setId(id);
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setStartDate(ConsoleHelperUtil.formattedData(dataStart));
        project.setEndDate(ConsoleHelperUtil.formattedData(dataEnd));
        project.setUserId(userId);
        projectRepository.merge(id, project);
    }

    public final void clearProjectList() {
        projectRepository.removeAll();
    }

}
