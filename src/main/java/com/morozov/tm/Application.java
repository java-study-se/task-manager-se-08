package com.morozov.tm;

import com.morozov.tm.command.project.*;
import com.morozov.tm.command.system.AboutCommand;
import com.morozov.tm.command.system.HelpCommand;
import com.morozov.tm.command.task.*;
import com.morozov.tm.command.user.*;
import com.morozov.tm.service.Bootstrap;

public class Application {
    private static final Class[] CLASSES = {
            HelpCommand.class, AboutCommand.class,
            ProjectClearCommand.class, ProjectCreateCommand.class, ProjectListCommand.class,
            ProjectRemoveCommand.class, ProjectUpdateCommand.class,
            TaskClearCommand.class, TaskCreateCommand.class, TaskListByProjectIdCommand.class,
            TaskListCommand.class, TaskRemoveCommand.class, TaskUpdateCommand.class,
            UserLoginCommand.class, UserLogoutCommand.class, UserRegistryCommand.class,
            UserShowCommand.class, UserUpdatePasswordCommand.class, UserUpdateProfileCommand.class
    };

    public static void main(String[] args) {
        new Bootstrap().init(CLASSES);
    }
}
