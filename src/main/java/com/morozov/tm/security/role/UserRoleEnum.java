package com.morozov.tm.security.role;

public enum UserRoleEnum {
    ADMIN("Администратор"), USER("Пользователь"), ALL("Общий");
    private String displayName;

    public String getDisplayName() {
        return displayName;
    }

    UserRoleEnum(String displayName) {
        this.displayName = displayName;
    }
}
