package com.morozov.tm.api;

import java.util.List;

public interface IUserRepository<T> {
    List<T> findAll();

    T findOne(String id);

    T findOneByLogin(String login);

    void persist(String id, T user);

    void merge(String id, T updateUser);

    void remove(String id);

    void removeAll();
}

