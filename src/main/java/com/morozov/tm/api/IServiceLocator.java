package com.morozov.tm.api;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.service.ProjectService;
import com.morozov.tm.service.TaskService;
import com.morozov.tm.entity.User;
import com.morozov.tm.service.UserService;

import java.util.List;

public interface IServiceLocator {
    List<AbstractCommand> getCommandList();

    ProjectService getProjectService();

    TaskService getTaskService();

    UserService getUserService();

    User getCurrentUser();

    void setCurrentUser(User user);
}
