package com.morozov.tm.repository;

import com.morozov.tm.api.ITaskRepository;
import com.morozov.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository<Task> {

    @NotNull
    @Override
    public List<Task> findAllByProjectIdUserId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> resultListByUserId = new ArrayList<>();
        for (Task task : getAllTaskByProjectId(projectId)) {
            if (task.getUserId().equals(userId)) resultListByUserId.add(task);
        }
        return resultListByUserId;
    }
    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) {
        final List<Task> resultTaskList = new ArrayList<>();
        for (Task task : findAll()) {
            if (task.getUserId().equals(userId)) resultTaskList.add(task);
        }
        return resultTaskList;
    }

    @Nullable
    @Override
    public Task findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        Task resultTask = null;
        for (Task task : findAllByUserId(userId)) {
            if (task.getId().equals(id)) resultTask = task;
        }
        return resultTask;
    }

    @NotNull
    @Override
    public List<Task> getAllTaskByProjectId(String projectId) {
        @NotNull List<Task> resultList = new ArrayList<>();
        for (Task task : entityMap.values()) {
            if (task.getIdProject().equals(projectId)) resultList.add(task);
        }
        return resultList;
    }

    @Override
    public void deleteAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> taskToDelete = findAllByProjectIdUserId(userId, projectId);
        for (Task task : taskToDelete) {
            remove(task.getId());
        }
    }

}
