package com.morozov.tm.repository;

import com.morozov.tm.api.IProjectRepository;
import com.morozov.tm.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository<Project> {
    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) {
        final List<Project> projectListByUserID = new ArrayList<>();
        for (Project project : findAll()) {
            if (project.getUserId().equals(userId)) projectListByUserID.add(project);
        }
        return projectListByUserID;
    }
    @Nullable
    @Override
    public Project findOneByUserId(@NotNull final String userId,@NotNull final String id) {
        Project resultProject = null;
        for (Project project : findAllByUserId(userId)) {
            if (project.getId().equals(id)) resultProject = project;
        }
        return resultProject;
    }
}
