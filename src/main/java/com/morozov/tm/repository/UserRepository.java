package com.morozov.tm.repository;

import com.morozov.tm.api.IUserRepository;
import com.morozov.tm.entity.User;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.MD5HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository<User> {

    public UserRepository() {
        final User admin = new User();
        admin.setLogin("admin");
        admin.setPasswordHash(MD5HashUtil.getHash("admin"));
        admin.setRole(UserRoleEnum.ADMIN);
        final User user = new User();
        user.setLogin("user");
        user.setPasswordHash(MD5HashUtil.getHash("user"));
        user.setRole(UserRoleEnum.USER);
        entityMap.put(admin.getId(), admin);
        entityMap.put(user.getId(), user);
    }
    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) {
        @NotNull final List<User> userList = findAll();
        User foundUser = null;
        for (User user : userList
        ) {
            if (user.getLogin().equals(login)) foundUser = user;
        }
        return foundUser;
    }
}



