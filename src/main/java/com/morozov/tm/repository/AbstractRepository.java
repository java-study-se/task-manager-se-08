package com.morozov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T> {
    @NotNull
    final Map<String, T> entityMap = new HashMap<>();
    @NotNull
    public List<T> findAll() {
        return new ArrayList<>(entityMap.values());
    }
    @Nullable
    public T findOne(@NotNull String id) {
        return entityMap.get(id);
    }

    public void merge(@NotNull String id, @NotNull T updateEntity) {
        entityMap.put(id, updateEntity);
    }

    public void persist(@NotNull String id,@NotNull T writeEntity) {
        entityMap.put(id, writeEntity);
    }

    public void remove(@NotNull String id) {
        entityMap.remove(id);
    }

    public void removeAll() {
        entityMap.clear();
    }
}
