package com.morozov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5HashUtil {
    public static String getHash(@NotNull final String password) {
        @Nullable MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(password.getBytes("utf-8"));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        @Nullable String resultMdHashPassword = null;
        if (messageDigest != null) {
            resultMdHashPassword = new BigInteger(1, messageDigest.digest()).toString(16);
        }
        if (resultMdHashPassword != null) {
            while (resultMdHashPassword.length() < 32) {
                resultMdHashPassword = "0" + resultMdHashPassword;
            }
        }
        return resultMdHashPassword;
    }
    }
