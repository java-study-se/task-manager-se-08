package com.morozov.tm.command;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.security.role.UserRoleEnum;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;


public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;
    protected List<UserRoleEnum> userRoleList = new ArrayList<>();

    public void setServiceLocator(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute();

    public List<UserRoleEnum> getUserRoleList() {
        return userRoleList;
    }
}
