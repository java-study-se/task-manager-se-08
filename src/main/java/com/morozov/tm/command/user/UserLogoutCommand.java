package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.User;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserLogoutCommand extends AbstractCommand {

    public UserLogoutCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Close current user session";
    }

    @Override
    final public void execute() {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        if (currentUser != null) {
            ConsoleHelperUtil.writeString(String.format("Сессия пользователя %s закрыта", currentUser.getLogin()));
        }
        serviceLocator.setCurrentUser(null);
    }
}
