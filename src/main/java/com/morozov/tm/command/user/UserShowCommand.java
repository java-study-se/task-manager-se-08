package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.User;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserShowCommand extends AbstractCommand {

    public UserShowCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "user-show";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Show current user";
    }

    @Override
    final public void execute() {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        if (currentUser != null) {
            ConsoleHelperUtil.writeString("Имя текущего пользователя: " + currentUser.getLogin());
            ConsoleHelperUtil.writeString("ID текущего пользователя: " + currentUser.getId());
            ConsoleHelperUtil.writeString("Права текущего пользователя: " + currentUser.getRole().getDisplayName());
        }
    }
}
