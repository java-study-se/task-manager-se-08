package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.MD5HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserUpdateProfileCommand extends AbstractCommand {

    public UserUpdateProfileCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "user-update-profile";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Update user profile";
    }

    @Override
    final public void execute() {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelperUtil.writeString("Введите старый пароль");
        @NotNull final String oldPassword = ConsoleHelperUtil.readString();

        if (currentUser != null && MD5HashUtil.getHash(oldPassword).equals(currentUser.getPasswordHash())) {
            ConsoleHelperUtil.writeString("Введите новое имя пользователя");
            @NotNull final String newUserName = ConsoleHelperUtil.readString();
            ConsoleHelperUtil.writeString("Введите новый пароль");
            @NotNull final String newUserPassword = ConsoleHelperUtil.readString();
            try {
                serviceLocator.getUserService().updateUserProfile(currentUser.getId(), newUserName, newUserPassword);
                ConsoleHelperUtil.writeString("Профиль обновлен");
            } catch (final StringEmptyException e) {
                ConsoleHelperUtil.writeString("Веденные строки не могут быть пустыми");
            } catch (final UserExistException e) {
                ConsoleHelperUtil.writeString("Пользователь с таким именем уже существует");
            } catch (UserNotFoundException e) {
                ConsoleHelperUtil.writeString("Пользователь не найден");
            }
        }
    }
}
