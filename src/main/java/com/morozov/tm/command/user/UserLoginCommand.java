package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserLoginCommand extends AbstractCommand {
    @NotNull
    @Override
    final public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "User authorisation";
    }

    @Override
    final public void execute() {
        ConsoleHelperUtil.writeString("Введите логин");
        @NotNull final String login = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите пароль");
        @NotNull final String password = ConsoleHelperUtil.readString();
        try {
            @Nullable final User authoriseUser = serviceLocator.getUserService().loginUser(login, password);
            if (authoriseUser != null) {
                serviceLocator.setCurrentUser(authoriseUser);
                ConsoleHelperUtil.writeString("Вы вошли под учетной записью " + authoriseUser.getLogin());
            } else {
                ConsoleHelperUtil.writeString("Некорректный пароль пользователя");
            }
        } catch (final UserNotFoundException e) {
            ConsoleHelperUtil.writeString("Пользователь не найден");
        } catch (final StringEmptyException e) {
            ConsoleHelperUtil.writeString("Введенные поля не могут быть пустими");
        }
    }
}
