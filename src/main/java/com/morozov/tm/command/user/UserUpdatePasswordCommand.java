package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.MD5HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserUpdatePasswordCommand extends AbstractCommand {

    public UserUpdatePasswordCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "user-password-update";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Update user password";
    }

    @Override
    final public void execute() {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelperUtil.writeString("Введите старый пароль");
        @NotNull final String oldPassword = ConsoleHelperUtil.readString();
        if (currentUser != null && MD5HashUtil.getHash(oldPassword).equals(currentUser.getPasswordHash())) {
            ConsoleHelperUtil.writeString("Введите новый пароль");
            @NotNull final String newPassword = ConsoleHelperUtil.readString();
            try {
                serviceLocator.getUserService().updateUserPassword(currentUser.getId(), newPassword);
                ConsoleHelperUtil.writeString("Пароль пользователя " + currentUser.getLogin() + " изменен");
            } catch (final StringEmptyException e) {
                ConsoleHelperUtil.writeString("Пароль не может быть пустым");
            } catch (UserNotFoundException e) {
                ConsoleHelperUtil.writeString("Пользователь не найден");
            }
        }
    }
}
