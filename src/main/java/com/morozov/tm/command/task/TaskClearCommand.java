package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;

public class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    final public void execute() {
        serviceLocator.getTaskService().clearTaskList();
        ConsoleHelperUtil.writeString("Список задач очищен");
    }
}
