package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;

public class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Create task";
    }

    @Override
    final public void execute() {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelperUtil.writeString("Введите имя новой задачи");
        @NotNull final String taskName = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите описание новой задачи");
        @NotNull final String taskDescription = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите дату начала новой задачи в формате DD.MM.YYYY");
        @NotNull final String startTaskDate = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите дату окончания новой задачи в формате DD.MM.YYYY");
        @NotNull final String endTaskDate = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите ID проекта задачи");
        @NotNull final String projectId = ConsoleHelperUtil.readString();
        if (currentUser != null) {
            try {
                @NotNull final Task task = serviceLocator.getTaskService().addTask(currentUser.getId(), taskName, taskDescription, startTaskDate, endTaskDate, projectId);
                ConsoleHelperUtil.writeString("Добавлена задача " + task.toString());
            } catch (final StringEmptyException e) {
                ConsoleHelperUtil.writeString("Введенные поля не могут быть пустими");
            } catch (final ParseException e) {
                ConsoleHelperUtil.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
            }
        }
    }
}
