package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class TaskListByProjectIdCommand extends AbstractCommand {

    public TaskListByProjectIdCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }
    @NotNull
    @Override
    final public String getName() {
        return "task-list-id";
    }
    @NotNull
    @Override
    final public String getDescription() {
        return "Show all task by selected project";
    }

    @Override
    final public void execute() {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelperUtil.writeString("Введите ID проекта");
        @NotNull final String projectId = ConsoleHelperUtil.readString();
        if (currentUser != null) {
            try {
                @NotNull final List<Task> taskList = serviceLocator.getTaskService().getAllTaskByProjectId(currentUser.getId(), projectId);
                ConsoleHelperUtil.writeString("Список задач по проекту с ID: " + projectId);
                for (int i = 0; i < taskList.size(); i++) {
                    ConsoleHelperUtil.writeString(String.format(("%d: %s"), i, taskList.get(i).toString()));
                }
            } catch (final StringEmptyException e) {
                ConsoleHelperUtil.writeString("Введенные поля не могут быть пустими");
            } catch (final RepositoryEmptyException e) {
                ConsoleHelperUtil.writeString("Список задач пуст");
            }
        }
    }
}
