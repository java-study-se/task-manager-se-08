package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;

public class TaskUpdateCommand extends AbstractCommand {

    public TaskUpdateCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Update selected task";
    }

    @Override
    final public void execute() {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelperUtil.writeString("Введите ID задачи для изменения");
        @NotNull final String updateTaskID = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новое имя задачи");
        @NotNull final String updateTaskName = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новое описание задачи");
        @NotNull final String updateTaskDescription = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новую дату начала задачи в формате DD.MM.YYYY");
        @NotNull final String startUpdateTaskDate = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новую дату окончания задачи в формате DD.MM.YYYY");
        @NotNull final String endUpdateTaskDate = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите ID проекта задачи");
        @NotNull final String updateTaskProjectId = ConsoleHelperUtil.readString();
        if (currentUser != null) {
            try {
                serviceLocator.getTaskService().updateTask(currentUser.getId(), updateTaskID, updateTaskName,
                        updateTaskDescription, startUpdateTaskDate, endUpdateTaskDate, updateTaskProjectId);
                ConsoleHelperUtil.writeString("Задача с порядковым номером " + updateTaskID + " обновлена");
            } catch (final RepositoryEmptyException e) {
                ConsoleHelperUtil.writeString("Список задач пуст");
            } catch (final StringEmptyException e) {
                ConsoleHelperUtil.writeString("Введенные поля не могут быть пустими");
            } catch (final ParseException e) {
                ConsoleHelperUtil.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
            }
        }
    }
}
