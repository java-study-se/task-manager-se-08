package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class TaskListCommand extends AbstractCommand {

    public TaskListCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Show all tasks";
    }

    @Override
    final public void execute() {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        if (currentUser != null) {
            try {
                @NotNull final List<Task> taskList = serviceLocator.getTaskService().getAllTaskByUserId(currentUser.getId());
                ConsoleHelperUtil.writeString("Список задач:");
                for (int i = 0; i < taskList.size(); i++) {
                    ConsoleHelperUtil.writeString(String.format(("%d: %s"), i, taskList.get(i).toString()));
                }
            } catch (final RepositoryEmptyException e) {
                ConsoleHelperUtil.writeString("Список задач пуст");
            }
        }
    }
}
