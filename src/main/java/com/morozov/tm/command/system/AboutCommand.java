package com.morozov.tm.command.system;

import com.jcabi.manifests.Manifests;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;

public class AboutCommand extends AbstractCommand {
    public AboutCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
        userRoleList.add(UserRoleEnum.ALL);
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "About program";
    }

    @Override
    public void execute() {
        ConsoleHelperUtil.writeString("Информация о приложении");
        @NotNull final String version = Manifests.read("BuildNumber");
        ConsoleHelperUtil.writeString("Версия сборки: " + version);
    }
}
