package com.morozov.tm.command.system;


import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;

public class HelpCommand extends AbstractCommand {

    public HelpCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
        userRoleList.add(UserRoleEnum.ALL);
    }

    @NotNull
    @Override
    final public String getName() {
        return "help";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Show all command";
    }

    @Override
    final public void execute() {
        for (@NotNull final AbstractCommand command : serviceLocator.getCommandList()) {
            ConsoleHelperUtil.writeString(String.format("%s: %s", command.getName(), command.getDescription()));
        }
    }
}
