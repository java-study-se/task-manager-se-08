package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;

public class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }
    @NotNull
    @Override
    final public String getName() {
        return "project-create";
    }
    @NotNull
    @Override
    final public String getDescription() {
        return "Create project";
    }

    @Override
    final public void execute() {
        ConsoleHelperUtil.writeString("Введите имя нового проекта");
        @NotNull final String projectName = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите описание нового проекта");
        @NotNull final String projectDescription = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите дату начала нового проекта в формате DD.MM.YYYY");
        @NotNull final String startProjectDate = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите дату окончания нового проекта в формате DD.MM.YYYY");
        @NotNull final String endProjectDate = ConsoleHelperUtil.readString();
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        try {
            assert currentUser != null;
            @NotNull final Project addedProject = serviceLocator.getProjectService().addProject(currentUser.getId(), projectName, projectDescription, startProjectDate, endProjectDate);
            ConsoleHelperUtil.writeString("Добавлен проект: " + addedProject.toString());
        } catch (final StringEmptyException e) {
            ConsoleHelperUtil.writeString("Введенные поля не могут быть пустими");
        } catch (final ParseException e) {
            ConsoleHelperUtil.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
        }
    }
}
