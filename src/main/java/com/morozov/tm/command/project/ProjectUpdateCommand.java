package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;

public class ProjectUpdateCommand extends AbstractCommand {
    public ProjectUpdateCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Update selected project";
    }

    @Override
    final public void execute() {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelperUtil.writeString("Введите ID проекта для изменения");
        @NotNull final String updateProjectId = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новое имя проекта");
        @NotNull final String updateProjectName = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новое описание проекта");
        @NotNull final String updateProjectDescription = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новую дату начала проекта в формате DD.MM.YYYY");
        @NotNull final String startUpdateDate = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новую дату окончания проекта в формате DD.MM.YYYY");
        @NotNull final String endUpdateDate = ConsoleHelperUtil.readString();
        if (currentUser != null) {
            try {
                serviceLocator.getProjectService().updateProject(currentUser.getId(), updateProjectId, updateProjectName, updateProjectDescription, startUpdateDate, endUpdateDate);
                ConsoleHelperUtil.writeString("Проект изменен");
            } catch (final RepositoryEmptyException e) {
                ConsoleHelperUtil.writeString("Список проектов пуст");
            } catch (final StringEmptyException e) {
                ConsoleHelperUtil.writeString("Введенные поля не могут быть пустими");
            } catch (final ParseException e) {
                ConsoleHelperUtil.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
            }
        }
    }
}
