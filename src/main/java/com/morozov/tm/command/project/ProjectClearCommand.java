package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;

public class ProjectClearCommand extends AbstractCommand {
    public ProjectClearCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @NotNull
    @Override
    final public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Remove all projects";
    }

    @Override
    final public void execute() {
        serviceLocator.getTaskService().clearTaskList();
        ConsoleHelperUtil.writeString("Список задач очищен");
        serviceLocator.getProjectService().clearProjectList();
        ConsoleHelperUtil.writeString("Список проектов очищен");
    }
}
