package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class ProjectListCommand extends AbstractCommand {
    public ProjectListCommand() {
        userRoleList.add(UserRoleEnum.USER);
        userRoleList.add(UserRoleEnum.ADMIN);
    }
    @NotNull
    @Override
    final public String getName() {
        return "project-list";
    }
    @NotNull
    @Override
    final public String getDescription() {
        return "Show all projects";
    }

    @Override
    final public void execute() {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        if(currentUser!= null) {
            try {
                @NotNull final List<Project> projectList = serviceLocator.getProjectService().getAllProjectByUserId(currentUser.getId());
                ConsoleHelperUtil.writeString("Список проектов пользователя " + currentUser.getLogin() + " :");
                for (int i = 0; i < projectList.size(); i++) {
                    ConsoleHelperUtil.writeString(String.format("%d: %s", i, projectList.get(i).toString()));
                }
            } catch (final RepositoryEmptyException e) {
                ConsoleHelperUtil.writeString("Список проектов пуст");
            }
        }
    }
}
